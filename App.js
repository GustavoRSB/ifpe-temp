import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import HomeScreen from './screens/screenList'
import EditScreen from './screens/screenEdit'
import CadastroScreen from './screens/screenForm'

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={ HomeScreen } options={{ headerShown: false }} />
        <Stack.Screen name="Editar" component={ EditScreen } options={{ headerShown: false }} />
        <Stack.Screen name="Cadastrar" component={ CadastroScreen } options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}