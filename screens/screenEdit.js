import React, { useEffect, useState } from 'react';
import { View, Text, TextInput } from 'react-native';
import { Thumbnail } from 'native-base'
import { Button } from 'react-native-elements';
import axios from 'axios';

export default function Editar({ route, navigation }) {

    const [getNome, setNome] = useState('');
    const [getCpf, setCpf] = useState('');
    const [getTelefone, setTelefone] = useState('');
    const [getId, setId] = useState('');

    useEffect(() => {
        if (route.params) {
            const { nome } = route.params
            const { cpf } = route.params
            const { telefone } = route.params
            const { id } = route.params

            setNome(nome);
            setTelefone(telefone);
            setCpf(cpf);
            setId(id);
        }

    }, [])

    async function editarDados() {
        const result = axios.put('http://professornilson.com/testeservico/clientes/' + getId, {
            nome: getNome,
            cpf: getCpf,
            telefone: getTelefone
        })
        
        console.log(result);

        navigation.navigate('Home')
    }

    async function excluirDados() {

        fetch('http://professornilson.com/testeservico/clientes/' + getId, {
            method: 'DELETE'
        });

        navigation.navigate('Home')
    }

    return (

        <View style={{ flex: 3, justifyContent: 'center' }}>

            <Thumbnail source={{ uri: 'https://reactnative.dev/img/tiny_logo.png' }} />

            <Text >Digite seu Nome</Text>
            <TextInput
                onChangeText={text => setNome(text)}
                value={getNome}
            />
            <Text >Digite seu Cpf</Text>
            <TextInput
                onChangeText={text => setCpf(text)}
                value={getCpf}
            />
            <Text >Digite seu Telefone</Text>
            <TextInput
                onChangeText={text => setTelefone(text)}
                value={getTelefone}
            />
            <Button
                title="Editar"
                onPress={() => editarDados()}

            />
            <Button
                title="Excluir"
                onPress={() => excluirDados()}
            />
        </View>
    );

}
